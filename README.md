# Offspring - Create users from CLI

This package adds a `make:user` artisan command, to create users on the command line. It is fully customizable (user model extra fields, user notification, random password generator).

Example:

```
$ php artisan make:user

 Name:
 > Arcesilas

 Email:
 > arcesilas@neutre.email

 Password (leave blank for a random password):
 >

Create a new user with these information:
Name: Arcesilas
Email: arcesilas@neutre.email
Password: *********

 Do you confirm the user creation? (yes/no) [yes]:
 > y

 Notify user once account is created? (yes/no) [yes]:
 > y

User will be notified once the account is created.
User created (ID: 1)
User has been notified
```

## Installation

Install with Composer:

```
composer require arcesilas\offspring
```

## Customize

If you wish to customize the package, publish the package files:

```
php artisan vendor:publish --tag=offspring
```

This will publish the configuration file and mail notification view.

### Configuration

The configuration file `config/offspring.php` is rather straight forward. However, here are some more details.

* `password-generator-provider`: When using random password generation, the command needs a password generator. It's provided by [hackzilla/password-generator](https://github.com/hackzilla/password-generator) package.

  Since the password generator may need some configuration, you cannot configure the generator, but the service provider which defines how it is instantiated.

* `show-password`: When set to true, the password is asked in clear text and displayd before creation confirmation. It's only usefull when you need to know immediately which password has been randomly generated, if so.

* `notification`: Inline doc is explicit enough

* `extra-fields`: If your User model has specific fields which are required for user to be created or if you want to set them at creation time, specify here their names with default value.

### Notification

By default, when a new user is created, a notification mail is sent on their email address, with a reset link to let them change their password easily.

To disable notifications, just set the `notification.class` option to `null`.

You can change the class to use your own notification, allowing you to customize the view and the channel (mail, sms, database, etc.)

## TODO

* Add option to specify the database connection
