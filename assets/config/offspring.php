<?php

return [
    /*
     * Password generator provider
     * Feel free to use your own to get another password provider
     * See https://github.com/hackzilla/password-generator for available password generators
     *
     * Default: Arcesilas\Offspring\Providers\PasswordGeneratorProvider::class
     */
    'password-generator-provider' => Arcesilas\Offspring\Providers\PasswordGeneratorProvider::class,

    /*
     * Whether to display the password when prompting and before user creation confirmation or not
     *
     * Default: false
     */
    'show-password' => false,

    'notification' => [
        /*
         * Prompt whether user should be notified or not
         * If set to `true` and `notification.class` is null, user will obviously not be notified
         *
         * Default: true
         */
        'prompt' => true,

        /*
         * The notification class to use to notify the user
         * Set to null to disable notification when user is created
         * If `notification.prompt` is set to `false`, user will be notified without prompting
         *
         * Default: Arcesilas\Offspring\Notifications\AccountCreated::class
         */
        'class' => Arcesilas\Offspring\Notifications\AccountCreated::class
    ],

    /*
     * Prompt for extra fields to create user with
     *
     * Default: []
     */
    'extra-fields' => [
        // 'field-name' => 'default-value'
    ],
];
