@component('mail::message')
# Welcome {{$user->name}}!

Your account on [{{config('app.name')}}]({{config('app.url')}}) has been created.

You may change your password by clicking on the following button.
@component('mail::button', ['url' => route('password.reset', $token)])
Change your password
@endcomponent

See you soon,<br>
{{ config('app.name') }}
@endcomponent
