<?php

namespace Arcesilas\Offspring\Console\Commands;

use Illuminate\Console\Command;

/**
 * This abstract class contains what can be considered as "helpers" for
 * the command which actually creates the user
 */
abstract class OffspringCommand extends Command
{
    /**
     * Whether the created user will benotified or not
     * @var bool
     */
    protected $notifyUser = false;

    /**
     * Prompt for email and check it's valid and not already registered
     * @return string The new user's email address
     */
    protected function promptForEmail()
    {
        $valid = false;
        do {
            try {
                $email = $this->ask("Email");
                $valid = $this->validateEmail($email);
            } catch (\InvalidArgumentException $e) {
                $this->error($e->getMessage());
            }
        } while (! $valid);

        return $email;
    }

    /**
     * Validate the email address: check it is valid and not already used
     * @param  string $email The email to validate
     * @return string        The email after validation
     */
    protected function validateEmail(string $email)
    {
        if (! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \InvalidArgumentException(sprintf('Email "%s" is invalid', $email));
        }

        if (app(config('auth.providers.users.model'))->where('email', $email)->exists()) {
            throw new \InvalidArgumentException(sprintf('Email "%s" is already registered', $email));
        }

        return true;
    }

    /**
     * Prompt for password with confirmation and potentially random generation
     * @param  int    $tries         How many tries to give a password and a mathing confirmation
     * @param  bool   $randomIfBlank Whether to generate a random password if no input is given
     * @return string                The hashed password
     */
    protected function promptForPassword(int $attempts = 3)
    {
        $count = 0;
        do {
            $prompt = 'Password (leave blank for a random password)';
            $password = config('offspring.show-password') ? $this->ask($prompt) : $this->secret($prompt);
            if (! $password) {
                return null;
            }

            $confirm = $this->secret('Confirm password');

            // Too many attempts
            if (++$count == $attempts) {
                throw new \InvalidArgumentException("Failed to confirm password after {$attempts} attempts", 1);
            }

            // Passwords do not match
            if ($confirm !== $password) {
                $this->error('Passwords do not match');
                continue;
            }

        } while ($confirm !== $password);

        return $password;
    }

    /**
     * Prompt whether the user should be notified or not
     * @return bool
     */
    protected function promptForNotification()
    {
        $prompt = config('offspring.notification.prompt', true);
        if ($prompt) {
            return $this->confirm('Notify user once account is created?', true);
        }
        return true;
    }

    /**
     * Display a summary of the data and ask for confirmation before creating the user
     * @param  array  $data The user's data
     * @return bool
     */
    protected function confirmData(array $data)
    {
        // Summary
        $this->info('Create a new user with these information:');
        $this->line('<comment>Name</comment>: ' . $data['name']);
        $this->line('<comment>Email</comment>: ' . $data['email']);
        $this->line('<comment>Password</comment>: '
            . (config('offspring.show-password', true) ? $data['password'] : '*********'));

        // Extra fields
        if (count($extraFields = config('offspring.extra-fields', []))) {
            $this->line('Extra fields');

            foreach ($extraFields as $field => $default) {
                if (array_key_exists($field, $data)) {
                    $this->line("{$field}: {$data[$field]}");
                }
            }
        }

        // Actually confirm
        if (! $this->confirm('Do you confirm the user creation?', true)) {
            return false;
        }

        // Prompt for notification
        if (config('offspring.notification.class')) {
            $this->notifyUser = $this->promptForNotification();
            $this->comment("User will be notified once the account is created.");
        }

        return true;
    }
}
