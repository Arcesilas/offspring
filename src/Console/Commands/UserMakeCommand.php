<?php

namespace Arcesilas\Offspring\Console\Commands;

use Arcesilas\Offspring\Contracts\NurseryInterface;
use Arcesilas\Offspring\Notifications\AccountCreated;
use Hackzilla\PasswordGenerator\Generator\PasswordGeneratorInterface;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Notification;

class UserMakeCommand extends OffspringCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "make:user";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new user';

    /**
     * Password generator instance
     * @var PasswordGeneratorInterface
     */
    protected $passwordGenerator;

    /**
     * A User model instance used to create the actual user
     * @var Illuminate\Auth\Authenticatable
     */
    protected $user;

    /**
     * Nursery for offspring we give birth to
     * @var Nursery
     */
    protected $nursery;

    /**
     * Create the command
     * @param PasswordGeneratorInterface $passwordGenerator
     */
    public function __construct(NurseryInterface $nursery, PasswordGeneratorInterface $passwordGenerator)
    {
        $this->nursery = $nursery;
        $this->passwordGenerator = $passwordGenerator;
        $this->user = app(config('auth.providers.users.model'));

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        $data = [];

        $data['name'] = $this->ask("Name");
        $data['email'] = $this->promptForEmail();

        // The try/catch block must be here, because it may end the command by returning an exit code
        try {
            $data['password'] = $this->promptForPassword() ?? $this->passwordGenerator->generatePasswords(1)[0];
        } catch (\InvalidArgumentException $e) {
            $this->error($e->getMessage());
            return $e->getCode();
        }

        // Extra fields
        foreach (config('offspring.extra-fields', []) as $field => $default) {
            if ($this->user->isFillable($field)) {
                $data[$field] = $this->ask("Extra field <comment>{$field}</comment>", $default);
            } else {
                $this->comment(sprintf('Skipping extra field "%s": not fillable', $field));
            }
        }

        // Confirmation
        if (false === $this->confirmData($data)) {
            $this->info('User creation aborted');
            return 2;
        }

        $this->createUser($data);
    }

    /**
     * Actually create and notify the user
     * @param  array $data User data
     */
    protected function createUser($data)
    {
        // Encrypt the password
        $data['password'] = bcrypt($data['password']);

        $user = $this->user->create($data);
        $this->info(sprintf('User created (ID: <comment>%s</comment>)', $user->id));

        $this->nursery->push($user);

        // Notify user, if need be
        if ($this->notifyUser) {
            $token = Password::createToken($user);
            $user->notify(new AccountCreated($token));
            $this->info('User has been notified');
        }
    }
}
