<?php

namespace Arcesilas\Offspring\Contracts;

use Illuminate\Support\Collection;

interface NurseryInterface
{
    /**
     * Get the last item from the collection.
     *
     * @param  callable|null  $callback
     * @param  mixed  $default
     * @return mixed
     *
     * @see \Illuminate\Support\Collection::last()
     */
    public function last(callable $callback = null, $default = null);

    /**
     * Push an item onto the end of the collection.
     *
     * @param  mixed  $value
     * @return $this
     *
     * @see \Illuminate\Support\Collection::push()
     */
    public function push($value);
}
