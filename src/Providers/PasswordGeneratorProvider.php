<?php

namespace Arcesilas\Offspring\Providers;

use Hackzilla\PasswordGenerator\Generator\HumanPasswordGenerator;
use Hackzilla\PasswordGenerator\Generator\PasswordGeneratorInterface;
use Illuminate\Support\ServiceProvider;

class PasswordGeneratorProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(PasswordGeneratorInterface::class, function ($app) {
            return (new HumanPasswordGenerator())
                ->setWordList(__DIR__.'/../../assets/dict.txt')
                ->setWordCount(3)
                ->setWordSeparator('-');
        });
    }
}
