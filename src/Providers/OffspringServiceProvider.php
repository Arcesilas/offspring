<?php

namespace Arcesilas\Offspring\Providers;

use Arcesilas\Offspring\Nursery;
use Arcesilas\Offspring\Contracts\NurseryInterface;
use Arcesilas\Offspring\Console\Commands\UserMakeCommand;
use Arcesilas\Offspring\Providers\PasswordGeneratorProvider;
use Illuminate\Support\ServiceProvider;

class OffspringServiceProvider extends ServiceProvider
{
    protected $assets = __DIR__.'/../../assets';

    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes(
                [$this->assets.'/config/offspring.php' => config_path('offspring.php')],
                'offspring'
            );

            $this->commands([
                UserMakeCommand::class,
            ]);

            $this->loadViewsFrom($this->assets.'/views/', 'offspring');
        }
    }

    public function register()
    {
        $this->mergeConfigFrom($this->assets.'/config/offspring.php', 'offspring');
        $this->app->register(PasswordGeneratorProvider::class);
        $this->app->singleton(NurseryInterface::class, Nursery::class);
    }
}
