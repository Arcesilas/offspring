<?php

namespace Arcesilas\Offspring\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Password;

class AccountCreated extends Notification
{
    /**
     * Password reset token, to allow user to change password in the first place
     * @var string
     */
    protected $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($user)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($user)
    {
        $data = [
            'user' => $user,
            'token' => $this->token
        ];

        return (new MailMessage)
            ->subject('Your account has been created on '.config('app.name'))
            ->markdown('offspring::emails.user-created', $data);
    }
}
