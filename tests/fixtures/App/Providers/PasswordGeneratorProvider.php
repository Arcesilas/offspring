<?php

namespace Arcesilas\Offspring\Tests\App\Providers;

use Hackzilla\PasswordGenerator\Generator\HumanPasswordGenerator;
use Hackzilla\PasswordGenerator\Generator\PasswordGeneratorInterface;
use Illuminate\Support\ServiceProvider;

class PasswordGeneratorProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(PasswordGeneratorInterface::class, function ($app) {
            return \Mockery::mock(HumanPasswordGenerator::class);
        });
    }
}
