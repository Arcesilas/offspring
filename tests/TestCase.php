<?php

namespace Arcesilas\Offspring\Tests;

use Arcesilas\Offspring\Tests\App\User;
use Hackzilla\PasswordGenerator\Generator\PasswordGeneratorInterface;
use MailThief\Testing\InteractsWithMail;
use Orchestra\Testbench\TestCase as OrchestraTestCase;

class TestCase extends OrchestraTestCase
{
    use InteractsWithMail;

    /**
     * Password generator
     * @var PasswordGeneratorInterface
     */
    protected $generator;

    /**
     * Setup the TestCase
     */
    protected function setUp()
    {
        parent::setUp();

        // Load the migrations
        $this->loadLaravelMigrations(['--database' => 'testbench']);
        $this->loadMigrationsFrom(__DIR__.'/fixtures/migrations');

        // Define a password.reset route, required for Password::
        app('router')->get('password/reset/{token}', function () {
            return redirect('/');
        })->name('password.reset');
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     */
    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'testbench');
        $app['config']->set('database.connections.testbench', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);

        $app['config']->set('auth.providers.users.model', User::class);
        $app['config']->set('offspring.extra-fields', ['nickname' => null, 'favorite_color' => 'red']);

        $this->generator = $app->make(PasswordGeneratorInterface::class);
    }

    /**
     * Load the providers.
     * Use annotations to allow a test to add/override a provider
     *
     * @param  Illuminate\Foundation\Application $app
     */
    protected function getPackageProviders($app)
    {
        $providers = array_values($this->getAnnotations()['method']['provider'] ?? []);

        return array_merge(
            [
                'Arcesilas\Offspring\Providers\OffspringServiceProvider',
            ],
            $providers
        );
    }
}
