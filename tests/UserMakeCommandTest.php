<?php

namespace Arcesilas\Offspring\Tests;

use Arcesilas\Offspring\Tests\App\User;
use Illuminate\Support\Facades\Hash;

class UserMakeCommandTest extends TestCase
{
    public function test_it_creates_a_user_with_a_given_password()
    {
        $this->artisan('make:user')
            ->expectsQuestion('Name', 'John Doe')
            ->expectsQuestion('Email', 'invalid@email')
            ->expectsOutput('Email "invalid@email" is invalid')
            ->expectsQuestion('Email', 'john.doe@example.com')
            ->expectsQuestion('Password (leave blank for a random password)', 'password')
            ->expectsQuestion('Confirm password', 'password')
            ->expectsQuestion('Extra field <comment>nickname</comment>', 'Johnny')
            ->expectsOutput('Skipping extra field "favorite_color": not fillable')
            ->expectsOutput('Create a new user with these information:')
            ->expectsOutput('Name: John Doe')
            ->expectsOutput('Email: john.doe@example.com')
            ->expectsOutput('Password: *********')
            ->expectsOutput('Extra fields')
            ->expectsOutput('nickname: Johnny')
            ->expectsQuestion('Do you confirm the user creation?', true)
            ->expectsQuestion('Notify user once account is created?', true)
            ->expectsOutput('User will be notified once the account is created.')
            ->expectsOutput('User created (ID: 1)')
            ->expectsOutput('User has been notified')
            ->run();

        $this->assertDatabaseHas('users', [
            'id' => 1,
            'email' => 'john.doe@example.com',
            'name' => 'John Doe',
            'nickname' => 'Johnny'
        ]);

        $user = User::find(1);
        $this->assertTrue(Hash::check('password', $user->password));

        $message = $this->getLastMessageFor('john.doe@example.com');

        $this->assertNotNull($message);
        $this->assertContains('Your account has been created on Laravel', $message->subject);
        $this->assertTrue($message->contains('Welcome '.$user->name));
    }

    public function test_it_fails_to_confirm_password_after_three_attempts()
    {
        $this->artisan('make:user')
            ->expectsQuestion('Name', 'John Doe')
            ->expectsQuestion('Email', 'john.doe@example.com')
            ->expectsQuestion('Password (leave blank for a random password)', 'password')
            ->expectsQuestion('Confirm password', 'pwd')
            ->expectsOutput('Passwords do not match')
            ->expectsQuestion('Password (leave blank for a random password)', 'password')
            ->expectsQuestion('Confirm password', 'pwd')
            ->expectsOutput('Passwords do not match')
            ->expectsQuestion('Password (leave blank for a random password)', 'password')
            ->expectsQuestion('Confirm password', 'pwd')
            ->expectsOutput('Failed to confirm password after 3 attempts')
            ->assertExitCode(1)
            ->run();

        $this->assertDatabaseMissing('users', [
            'id' => 1
        ]);
    }

    public function test_it_shows_error_if_email_is_already_registered()
    {
        User::create([
            'email' => 'john.doe@example.com',
            'name' => 'John Doe',
            'password' => Hash::make('password')
        ]);

        $this->assertDatabaseHas('users', [
            'id' => 1,
            'email' => 'john.doe@example.com',
            'name' => 'John Doe',
        ]);

        $this->artisan('make:user')
            ->expectsQuestion('Name', 'John Doe')
            ->expectsQuestion('Email', 'john.doe@example.com')
            ->expectsOutput('Email "john.doe@example.com" is already registered')
            ->expectsQuestion('Email', 'johnnydoe@example.com')
            ->expectsQuestion('Password (leave blank for a random password)', '')
            ->expectsQuestion('Extra field <comment>nickname</comment>', 'Johnny')
            ->expectsOutput('Skipping extra field "favorite_color": not fillable')
            ->expectsQuestion('Do you confirm the user creation?', true)
            ->expectsQuestion('Notify user once account is created?', false)
            ->run();

            $this->assertDatabaseHas('users', [
                'id' => 2,
                'email' => 'johnnydoe@example.com',
                'name' => 'John Doe',
            ]);
    }

    public function test_it_notifies_user_without_prompting()
    {
        app('config')->set('offspring.notification.prompt', false);

        $this->artisan('make:user')
            ->expectsQuestion('Name', 'John Doe')
            ->expectsQuestion('Email', 'invalid@email')
            ->expectsOutput('Email "invalid@email" is invalid')
            ->expectsQuestion('Email', 'john.doe@example.com')
            ->expectsQuestion('Password (leave blank for a random password)', 'password')
            ->expectsQuestion('Confirm password', 'password')
            ->expectsQuestion('Extra field <comment>nickname</comment>', 'Johnny')
            ->expectsOutput('Skipping extra field "favorite_color": not fillable')
            ->expectsOutput('Create a new user with these information:')
            ->expectsOutput('Name: John Doe')
            ->expectsOutput('Email: john.doe@example.com')
            ->expectsQuestion('Do you confirm the user creation?', true)
            ->expectsOutput('User will be notified once the account is created.')
            ->expectsOutput('User created (ID: 1)')
            ->expectsOutput('User has been notified')
            ->run();

        $this->assertDatabaseHas('users', [
            'id' => 1,
            'email' => 'john.doe@example.com',
            'name' => 'John Doe',
            'nickname' => 'Johnny'
        ]);
    }

    public function test_creation_aborts_if_not_confirmed()
    {
        $this->artisan('make:user')
            ->expectsQuestion('Name', 'John Doe')
            ->expectsQuestion('Email', 'john.doe@example.com')
            ->expectsQuestion('Password (leave blank for a random password)', 'password')
            ->expectsQuestion('Confirm password', 'password')
            ->expectsQuestion('Extra field <comment>nickname</comment>', 'Johnny')
            ->expectsOutput('Skipping extra field "favorite_color": not fillable')
            ->expectsOutput('Create a new user with these information:')
            ->expectsOutput('Name: John Doe')
            ->expectsOutput('Email: john.doe@example.com')
            ->expectsOutput('Password: *********')
            ->expectsOutput('Extra fields')
            ->expectsOutput('nickname: Johnny')
            ->expectsQuestion('Do you confirm the user creation?', false)
            ->expectsOutput('User creation aborted')
            ->assertExitCode(2)
            ->run();
    }

    /**
     * @provider Arcesilas\Offspring\Tests\App\Providers\PasswordGeneratorProvider
     */
    public function test_random_password_is_generated_if_none_is_given()
    {
        $this->generator->shouldReceive('generatePasswords')
            ->once()
            ->with(1)
            ->andReturn(['random-test-password']);

        $this->artisan('make:user')
            ->expectsQuestion('Name', 'John Doe')
            ->expectsQuestion('Email', 'john.doe@example.com')
            ->expectsQuestion('Password (leave blank for a random password)', '')
            ->expectsQuestion('Extra field <comment>nickname</comment>', 'Johnny')
            ->expectsOutput('Skipping extra field "favorite_color": not fillable')
            ->expectsOutput('Create a new user with these information:')
            ->expectsOutput('Name: John Doe')
            ->expectsOutput('Email: john.doe@example.com')
            ->expectsOutput('Password: *********')
            ->expectsOutput('Extra fields')
            ->expectsOutput('nickname: Johnny')
            ->expectsQuestion('Do you confirm the user creation?', true)
            ->expectsQuestion('Notify user once account is created?', false)
            ->expectsOutput('User created (ID: 1)')
            ->run();

        $this->assertDatabaseHas('users', [
            'id' => 1,
            'email' => 'john.doe@example.com',
            'name' => 'John Doe',
            'nickname' => 'Johnny'
        ]);

        $user = User::find(1);
        $this->assertTrue(
            Hash::check('random-test-password', $user->password),
            'Failed to check stored password is correct'
        );
    }
}
